import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Sidebar from "./components/Sidebar";
import Nav from "./components/Nav";
import Dashboard from "./components/Dashboard";
import Usuarios from "./components/Usuarios";
import UsuarioNovo from "./components/UsuarioNovo";
import UsuariosEditar from "./components/UsuariosEditar";
import Perfil from "./components/Perfil";
import PerfilNovo from "./components/PerfilNovo";
import PerfilEditar from "./components/PerfilEditar";
import Bordero from "./components/Bordero";
import BorderoLista from "./components/BorderoLista";

import MinhaConta from "./components/MinhaConta";

import Login from "./components/Login";
import Recuperar from "./components/Recuperar";

//Alert
import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
const options = {
  position: "bottom right",
  timeout: 3000,
  offset: "20px",
  transition: "scale"
};

//Criando rota privada - acessa após login efetuado
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem("token") ? (
        <Component
          {...props}
          perfil={JSON.parse(localStorage.getItem("perfis"))}
        />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const regraPerfil = JSON.parse(localStorage.getItem("perfis"));

class App extends Component {
  state = {
    logado: true
  };

  componentDidMount() {
    console.log(regraPerfil);
  }

  render() {
    return (
      <Router>
        <AlertProvider template={AlertTemplate} {...options}>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/recuperar" component={Recuperar} />
            <div className="wrapper ">
              <Sidebar />

              <div className="main-panel">
                <Nav />
                <PrivateRoute exact path="/" component={Dashboard} />

                {regraPerfil && regraPerfil.usuarios !== 0 ? (
                  <Switch>
                    <PrivateRoute
                      exact
                      path="/usuarios"
                      component={Usuarios}
                      teste="teste"
                    />
                    <PrivateRoute
                      path="/usuarios/editar/:id"
                      component={UsuariosEditar}
                    />
                    <PrivateRoute
                      path="/usuarios/novo"
                      component={UsuarioNovo}
                    />
                    <PrivateRoute exact path="/perfil" component={Perfil} />
                    <PrivateRoute
                      exact
                      path="/perfil/novo"
                      component={PerfilNovo}
                    />
                    <PrivateRoute
                      exact
                      path="/perfil/editar/:id"
                      component={PerfilEditar}
                    />
                  </Switch>
                ) : (
                  ""
                )}

                {regraPerfil && regraPerfil.bordero !== 0 ? (
                  <Switch>
                    <PrivateRoute exact path="/bordero" component={Bordero} />
                    <PrivateRoute
                      exact
                      path="/bordero/:codigo"
                      component={BorderoLista}
                    />
                  </Switch>
                ) : (
                  ""
                )}

                <PrivateRoute path="/minha-conta" component={MinhaConta} />
              </div>
            </div>
            <Route exact path="*" component={() => "Page Not Found (404)"} />
          </Switch>
        </AlertProvider>
      </Router>
    );
  }
}

export default App;

import axios from "axios";

const api_protheus = axios.create({
  baseURL: "http://192.168.0.13:9099/rest/"
});

export default api_protheus;

"use strict";

import React from "react";
import { Link } from "react-router-dom";
import { withAlert } from "react-alert";

import apiProtheus from "../api_protheus";

import _ from "lodash";
import moment from "moment";

class Bordero extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      borderos: []
    };
  }
  componentDidMount = async () => {
    const response = await apiProtheus.get(
      "http://192.168.0.13:9099/rest/titulo"
    );

    const agrupar = lista => {
      return lista.reduce((prev, item) => {
        const bordero = item.BORDERO;
        if (prev[bordero]) {
          prev[bordero].push(item);
        } else {
          prev[bordero] = [item];
        }
        return prev;
      }, []);
    };
    this.setState({
      borderos: Object.values(agrupar(response.data.TITULOS))
    });
  };

  sincronizar = async e => {
    const response = await apiProtheus.get(
      "http://192.168.0.13:9099/rest/titulo"
    );

    const agrupar = lista => {
      return lista.reduce((prev, item) => {
        const bordero = item.BORDERO;
        if (prev[bordero]) {
          prev[bordero].push(item);
        } else {
          prev[bordero] = [item];
        }
        return prev;
      }, []);
    };
    this.setState({
      borderos: Object.values(agrupar(response.data.TITULOS))
    });
    this.props.alert.success("Registros atualizados");
  };

  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <button className="btn btn-info" onClick={this.sincronizar}>
            <i className="material-icons">cached</i> Atualizar registros
          </button>

          <div className="card">
            <div className="card-header card-header-success">
              <h4 className="card-title">Borderô</h4>
            </div>
            <div className="card-body">
              <div className="table-responsive">
                <table className="table table-hover">
                  <thead className="text-warning">
                    <tr>
                      <th>Lote</th>
                      <th>Data</th>
                      <th>Item Contábil</th>
                      <th>Status</th>
                      <th>Emitente</th>
                      <th>Ações</th>
                    </tr>
                  </thead>

                  <tbody>
                    {this.state.borderos.map(list => (
                      <tr key={Math.random()}>
                        <td>{list[0].BORDERO}</td>
                        <td>{moment(list[0].EMISSAO).format("DD/MM/YYYY")}</td>
                        <td>{list[0].FORNECEDOR}</td>
                        <td>Aguardando</td>
                        <td>{list[0].EMITENTE}</td>
                        <td>
                          <Link
                            to={`/bordero/${list[0].BORDERO}`}
                            className="text-success"
                          >
                            <i className="material-icons">find_in_page</i>
                          </Link>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withAlert(Bordero);

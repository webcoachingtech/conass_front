import React from "react";
import { Link } from "react-router-dom";

import apiProtheus from "../api_protheus";
import moment from "moment";

//Formata valor
const formatar = new Intl.NumberFormat("pt-BR", {
  style: "currency",
  currency: "BRL"
});

class BorderoLista extends React.Component {
  state = {
    codigo: this.props.match.params.codigo,
    titulos: [],
    aprovados: [],
    reprovados: [],
    total: 0
  };

  componentDidMount = async () => {
    const response = await apiProtheus.get("/titulo");
    console.log(response.data);
    this.setState({
      titulos: response.data.TITULOS.filter(
        item => item.BORDERO === this.state.codigo
      )
    });
  };

  componentWillMount() {
    const total = this.state.aprovados.length + this.state.reprovados.length;
    this.setState({
      total
    });
  }

  aprovar = id => {
    const arr = this.state.aprovados;
    const arrReprovados = this.state.reprovados;

    if (!arr.includes(id)) {
      arr.push(id);
      if (arrReprovados.includes(id)) {
        const indexReprovados = arrReprovados.indexOf(id);
        arrReprovados.splice(indexReprovados, 1);
        this.setState({ reprovados: arrReprovados });
      }
    } else {
      const index = arr.indexOf(id);
      arr.splice(index, 1);
    }
    this.setState({ aprovados: arr });
    console.log(arr);

    const total = this.state.aprovados.length + this.state.reprovados.length;
    this.setState({
      total
    });
  };

  reprovar = id => {
    const arr = this.state.reprovados;
    const arrAprovados = this.state.aprovados;

    if (!arr.includes(id)) {
      arr.push(id);
      if (arrAprovados.includes(id)) {
        const indexAprovados = arrAprovados.indexOf(id);
        arrAprovados.splice(indexAprovados, 1);
        this.setState({ reprovados: arrAprovados });
      }
    } else {
      const index = arr.indexOf(id);
      arr.splice(index, 1);
    }
    this.setState({ reprovados: arr });
    console.log(arr);

    const total = this.state.aprovados.length + this.state.reprovados.length;
    this.setState({
      total
    });
  };

  salvarProtheus = async e => {
    e.preventDefault();
    try {
      const res = await apiProtheus.put("/titulo", {
        CODIGO: this.state.codigo,
        APROV: localStorage.getItem("nomeUsuario")
      });
      console.log(res);
    } catch (err) {
      alert(`Erro ao enviar: ${err}`);
    }
  };

  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <Link to="/bordero" className="btn btn-warning">
            <i className="material-icons">arrow_back</i>
          </Link>
          <div className="card">
            <div className="card-header card-header-success">
              <h4 className="card-title">
                Títulos (Bordero: {this.state.codigo})
              </h4>
            </div>
            <div className="card-body">
              <div className="table-responsive">
                <table className="table table-hover">
                  <thead className="text-warning">
                    <tr>
                      <th>Nº título</th>
                      <th>Fornecedor</th>
                      <th>Histórico</th>
                      <th>Valor</th>
                      <th>Venc. Real</th>
                      <th>Aprovação</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.titulos.map(list => (
                      <tr key={Math.random()}>
                        <td>{list.NUMERO}</td>
                        <td>{list.FORNECEDOR}</td>
                        <td>{list.HISTORICO}</td>
                        <td>{formatar.format(list.VALOR)}</td>
                        <td>{moment(list.VENCTO).format("DD/MM/YYYY")}</td>
                        <td width="15%">
                          <button
                            className={
                              this.state.aprovados.includes(list.NUMERO)
                                ? "btn btn-aprovacao btn-success"
                                : "btn btn-aprovacao"
                            }
                            onClick={() => this.aprovar(list.NUMERO)}
                          >
                            <i className="material-icons">check_circle</i>
                          </button>
                          <button
                            className={
                              this.state.reprovados.includes(list.NUMERO)
                                ? "btn btn-aprovacao btn-danger"
                                : "btn btn-aprovacao"
                            }
                            onClick={() => this.reprovar(list.NUMERO)}
                          >
                            <i className="material-icons">block</i>
                          </button>
                        </td>
                        <td width="20%">
                          {this.state.aprovados.includes(list.NUMERO) ? (
                            <b style={{ color: "#5c9e31" }}>SELECIONADO</b>
                          ) : (
                            ""
                          )}
                          {this.state.reprovados.includes(list.NUMERO) ? (
                            <b style={{ color: "#d13030" }}>RECUSADO</b>
                          ) : (
                            ""
                          )}
                        </td>
                      </tr>
                    ))}
                    {this.state.titulos.length === this.state.total ? (
                      <tr>
                        <td
                          colSpan="6"
                          style={{
                            fontWeight: "300",
                            fontStyle: "italic"
                          }}
                        >
                          OBS:{" "}
                          {this.state.reprovados.length +
                            this.state.aprovados.length ===
                            this.state.total &&
                          this.state.aprovados.length >
                            this.state.reprovados.length
                            ? `O Bordero (${
                                this.state.codigo
                              }) será APROVADO após enviar a solicitação!`
                            : `O Bordero (${
                                this.state.codigo
                              }) será REPROVADO após enviar a solicitação!`}
                        </td>
                        <td width="20%">
                          <button
                            className="btn btn-success"
                            onClick={this.salvarProtheus}
                          >
                            Enviar solicitação
                          </button>
                        </td>
                      </tr>
                    ) : (
                      ""
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BorderoLista;

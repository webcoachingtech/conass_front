import React from "react";
import { Link } from "react-router-dom";

class Dashboard extends React.Component {
  logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("nomeUsuario");
    this.props.history.push("/login");
  };
  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <div className="col-md-12">
            <div className="card card-profile">
              <div className="card-avatar">
                <img className="img" src="../../img/avatar.png" />
              </div>
              <div className="card-body">
                <h4 className="card-title">
                  Seja bem vindo(a),{" "}
                  <b>{localStorage.getItem("nomeUsuario")}</b>
                </h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;

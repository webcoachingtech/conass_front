import React from "react";
import { Link } from "react-router-dom";
import { withAlert } from "react-alert";

import jwtdecode from "jwt-decode";

import api from "../api";
import Perfil from "./Perfil";

class Login extends React.Component {
  state = {
    email: "",
    password: ""
  };

  logar = async () => {
    const res = await api.post("autenticar", {
      email: this.state.email,
      password: this.state.password
    });

    if (res.data.token && jwtdecode(res.data.token).data.status == 1) {
      const idUser = jwtdecode(res.data.token).data.id;
      const users = await api.get(`usuario/${idUser}`);
      const perfis = users.data[0].perfils;

      localStorage.setItem("perfis", JSON.stringify(perfis));
      const p = localStorage.getItem("perfis");

      localStorage.setItem("token", res.data.token);
      localStorage.setItem("nomeUsuario", jwtdecode(res.data.token).data.nome);
      this.props.alert.success("Login efetuado com sucesso");
      window.location.href = "/";
    } else {
      this.props.alert.error("Erro ao efetuar login");
    }
  };

  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <div className="login">
            <div className="row">
              <img src="../../img/logo-conass.png" alt="" width="100%" />
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="form-group">
                  <label className="bmd-label-floating">E-mail</label>
                  <input
                    type="text"
                    className="form-control"
                    onChange={e => this.setState({ email: e.target.value })}
                    value={this.state.email}
                  />
                </div>
              </div>
              <div className="col-md-12">
                <div className="form-group">
                  <label className="bmd-label-floating">Senha</label>
                  <input
                    type="password"
                    className="form-control"
                    onChange={e => this.setState({ password: e.target.value })}
                    value={this.state.password}
                  />
                </div>
              </div>
              <div className="col-md-12">
                <Link to="/recuperar" className="float-right txt-recuperar">
                  Esqueceu sua senha?
                </Link>
              </div>
              <div className="col-md-12">
                <button className="btn btn-success" onClick={this.logar}>
                  Entrar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withAlert(Login);

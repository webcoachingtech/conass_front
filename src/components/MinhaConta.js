import React from "react";
import { Link } from "react-router-dom";

const MinhaConta = props => {
  return (
    <div className="content animated fadeIn">
      <div className="container-fluid">
        <div className="card">
          <div className="card-header card-header-success">
            <h4 className="card-title">Minha Conta</h4>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label className="bmd-label-floating">
                    Senha (Alterar minha senha)
                  </label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-12">
                <button className="btn btn-info float-right">
                  Alterar senha
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MinhaConta;

import React from "react";
import { Link, withRouter } from "react-router-dom";

class Nav extends React.Component {
  logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("nomeUsuario");
    localStorage.removeItem("perfis");
    //this.props.history.push("/login");
    window.location.href = "/login";
  };
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            aria-controls="navigation-index"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="sr-only">Toggle navigation </span>
            <span className="navbar-toggler-icon icon-bar" />
            <span className="navbar-toggler-icon icon-bar" />
            <span className="navbar-toggler-icon icon-bar" />
          </button>
          <div className="collapse navbar-collapse justify-content-end">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link
                  className="nav-link"
                  to="/minha-conta"
                  title="Minha conta"
                  style={
                    this.props.location.pathname === "/minha-conta"
                      ? styles.negrito
                      : styles.semnegrito
                  }
                >
                  <i className="material-icons">person</i> Minha conta
                </Link>
              </li>
              <li className="nav-item">
                <button
                  className="btn btn-danger btn-round"
                  onClick={this.logout}
                >
                  Sair do sistema
                </button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

const styles = {
  negrito: {
    fontWeight: "bold",
    color: "#fff",
    background: "#ff9e0f"
  },
  semnegrito: {
    fontWeight: 100
  }
};

export default withRouter(Nav);

import React from "react";
import { Link } from "react-router-dom";
import { withAlert } from "react-alert";

import api from "../api";

class Perfil extends React.Component {
  state = {
    perfil: []
  };

  componentDidMount = async () => {
    const res = await api.get("perfil");
    this.setState({
      perfil: res.data
    });
  };

  apagar = async id => {
    try {
      const res = await api.delete(`perfil/${id}`);
      this.setState({
        perfil: this.state.perfil.filter(e => e.id !== id)
      });
      this.props.alert.success("Realizado com sucesso");
    } catch (err) {
      this.props.alert.error("Erro na solicitação");
    }
  };

  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <Link to="perfil/novo" className="btn btn-success">
            <i className="material-icons">add_circle</i>
          </Link>

          <div className="card">
            <div className="card-header card-header-success">
              <h4 className="card-title">Perfil</h4>
            </div>
            <div className="card-body">
              <div className="table-responsive">
                <table className="table table-hover">
                  <thead className="text-warning">
                    <tr>
                      <th>Nome</th>
                      <th>Ação</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.perfil.map(item => (
                      <tr key={item.id}>
                        <td>{item.nome}</td>
                        <td width="10%">
                          <Link to="/perfil/editar/1" className="text-success">
                            <i className="material-icons">edit</i>
                          </Link>
                          <a
                            className="text-success"
                            style={{ cursor: "pointer" }}
                            onClick={() => {
                              if (window.confirm("Deseja realmente apagar?")) {
                                return this.apagar(item.id);
                              }
                              return;
                            }}
                          >
                            <i className="material-icons">delete</i>
                          </a>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withAlert(Perfil);

import React from "react";
import { Link } from "react-router-dom";

const PerfilEditar = props => {
  return (
    <div className="content animated fadeIn">
      <div className="container-fluid">
        <Link to="/perfil" className="btn btn-warning">
          <i className="material-icons">format_align_justify</i>
        </Link>
        <div className="card">
          <div className="card-header card-header-success">
            <h4 className="card-title">Editar Perfil</h4>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-md-12">
                <div className="form-group">
                  <label className="bmd-label-floating">Nome</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-12">
                <h4>Permissão</h4>
              </div>

              <div className="col-md-2">
                <div className="form-group">
                  <label className="form-check-label">
                    <div className="form-check">
                      <input type="checkbox" className="form-check-input" />
                      <span className="form-check-sign">
                        <span className="check" /> Usuários
                      </span>
                    </div>
                  </label>
                </div>
              </div>

              <div className="col-md-3">
                <div className="form-group">
                  <label className="form-check-label">
                    <div className="form-check">
                      <input type="checkbox" className="form-check-input" />
                      <span className="form-check-sign">
                        <span className="check" /> Relatórios
                      </span>
                    </div>
                  </label>
                </div>
              </div>

              <div className="col-md-3">
                <div className="form-group">
                  <label className="form-check-label">
                    <div className="form-check">
                      <input type="checkbox" className="form-check-input" />
                      <span className="form-check-sign">
                        <span className="check" /> Borderò (Visualizador)
                      </span>
                    </div>
                  </label>
                </div>
              </div>

              <div className="col-md-3">
                <div className="form-group">
                  <label className="form-check-label">
                    <div className="form-check">
                      <input type="checkbox" className="form-check-input" />
                      <span className="form-check-sign">
                        <span className="check" /> Borderô (Aprovador)
                      </span>
                    </div>
                  </label>
                </div>
              </div>

              <div className="col-md-12">
                <button className="btn btn-info float-right">Cadastrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PerfilEditar;

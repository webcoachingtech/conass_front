import React from "react";
import { Link } from "react-router-dom";
import { withAlert } from "react-alert";

import api from "../api";

class PerfilNovo extends React.Component {
  state = {
    nome: "",
    usuarios: "",
    bordero: "",
    bordero_visualizador: "",
    bordero_aprovador: "",
    relatorios: ""
  };
  adicionar = () => {
    try {
      api.post("perfil", {
        nome: this.state.nome,
        usuarios: this.state.usuarios,
        bordero: this.state.bordero,
        bordero_visualizador: this.state.bordero_visualizador,
        bordero_aprovador: this.state.bordero_aprovador,
        relatorios: this.state.relatorios
      });
      this.props.alert.success("Realizado com sucesso!");
    } catch (err) {
      this.props.alert.error("Erro na solicitação");
    }
  };
  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <Link to="/perfil" className="btn btn-warning">
            <i className="material-icons">format_align_justify</i>
          </Link>
          <div className="card">
            <div className="card-header card-header-success">
              <h4 className="card-title">Novo Perfil</h4>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label className="bmd-label-floating">Nome</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e => this.setState({ nome: e.target.value })}
                      value={this.state.nome}
                    />
                  </div>
                </div>

                <div className="col-md-12">
                  <h4>Permissão</h4>
                </div>

                <div className="col-md-2">
                  <div className="form-group">
                    <label className="form-check-label">
                      <div className="form-check">
                        <input
                          type="checkbox"
                          value="1"
                          className="form-check-input"
                          onChange={e =>
                            this.setState({ usuarios: e.target.value })
                          }
                        />
                        <span className="form-check-sign">
                          <span className="check" /> Usuários
                        </span>
                      </div>
                    </label>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group">
                    <label className="form-check-label">
                      <div className="form-check">
                        <input
                          type="checkbox"
                          value="1"
                          className="form-check-input"
                          onChange={e =>
                            this.setState({ bordero: e.target.value })
                          }
                        />
                        <span className="form-check-sign">
                          <span className="check" /> Borderô
                        </span>
                      </div>
                    </label>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group">
                    <label className="form-check-label">
                      <div className="form-check">
                        <input
                          type="checkbox"
                          value="1"
                          className="form-check-input"
                          onChange={e =>
                            this.setState({ relatorios: e.target.value })
                          }
                        />
                        <span className="form-check-sign">
                          <span className="check" /> Relatórios
                        </span>
                      </div>
                    </label>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group">
                    <label className="form-check-label">
                      <div className="form-check">
                        <input
                          type="checkbox"
                          value="1"
                          className="form-check-input"
                          onChange={e =>
                            this.setState({
                              bordero_visualizador: e.target.value
                            })
                          }
                        />
                        <span className="form-check-sign">
                          <span className="check" /> Borderô (Visualizador)
                        </span>
                      </div>
                    </label>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group">
                    <label className="form-check-label">
                      <div className="form-check">
                        <input
                          type="checkbox"
                          value="1"
                          className="form-check-input"
                          onChange={e =>
                            this.setState({ bordero_aprovador: e.target.value })
                          }
                        />
                        <span className="form-check-sign">
                          <span className="check" /> Borderô (Aprovador)
                        </span>
                      </div>
                    </label>
                  </div>
                </div>

                <div className="col-md-12">
                  <button
                    className="btn btn-info float-right"
                    onClick={this.adicionar}
                  >
                    Cadastrar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withAlert(PerfilNovo);

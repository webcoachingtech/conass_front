import React from "react";
import { Link } from "react-router-dom";

const Base = props => {
  return (
    <div className="content animated fadeIn">
      <div className="container-fluid">
        <div className="login">
          <div className="login-title">Recupere sua senha</div>
          <div className="row">
            <div className="col-md-12">
              <div className="form-group">
                <label className="bmd-label-floating">Digite seu e-mail</label>
                <input type="text" className="form-control" />
              </div>
            </div>
            <div className="col-md-12">
              <Link to="/login" className="float-right txt-recuperar">
                {"<- Efetuar login"}
              </Link>
            </div>
            <div className="col-md-12">
              <button className="btn btn-success">Recuperar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Base;

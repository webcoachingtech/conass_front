import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import Menu from "../container/Menu";

const regraPerfil = JSON.parse(localStorage.getItem("perfis"));

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div
        className="sidebar"
        data-color="orange"
        data-background-color="white"
      >
        <div className="logo">
          <div className="simple-text logo-normal">
            <img src="../../img/logo-conass.png" alt="" width="100%" />
          </div>
        </div>
        <div className="sidebar-wrapper">
          <ul className="nav">
            {/*Menu.map(m => (
              <li
                className={
                  "/" + this.props.location.pathname.split("/")[1] === m.path
                    ? "nav-item active"
                    : "nav-item"
                }
                key={m.name}
              >
                <Link className="nav-link" to={m.path}>
                  <i className="material-icons">{m.icon}</i>
                  <p>{m.name}</p>
                </Link>
              </li>
              ))*/}
            <li
              className={
                this.props.location.pathname.split("/")[1] === ""
                  ? "nav-item active"
                  : "nav-item"
              }
            >
              <Link className="nav-link" to="/">
                <i className="material-icons">dashboard</i>
                <p>Dashboard</p>
              </Link>
            </li>

            {regraPerfil && regraPerfil.usuarios !== 0 ? (
              <div>
                <li
                  className={
                    this.props.location.pathname.split("/")[1] === "usuarios"
                      ? "nav-item active"
                      : "nav-item"
                  }
                >
                  <Link className="nav-link" to="/usuarios">
                    <i className="material-icons">person</i>
                    <p>Usuários</p>
                  </Link>
                </li>

                <li
                  className={
                    this.props.location.pathname.split("/")[1] === "perfil"
                      ? "nav-item active"
                      : "nav-item"
                  }
                >
                  <Link className="nav-link" to="/perfil">
                    <i className="material-icons">check_circle</i>
                    <p>Perfil</p>
                  </Link>
                </li>
              </div>
            ) : (
              ""
            )}

            <li
              className={
                this.props.location.pathname.split("/")[1] === "bordero"
                  ? "nav-item active"
                  : "nav-item"
              }
            >
              <Link className="nav-link" to="/bordero">
                <i className="material-icons">line_weight</i>
                <p>Borderô</p>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default withRouter(Sidebar);

import React from "react";
import { Link } from "react-router-dom";
import { withAlert } from "react-alert";

import api from "../api";

class UsuarioNovo extends React.Component {
  state = {
    perfil: [],
    nome: "",
    username: "",
    email: "",
    cpf: "",
    nomeacao: "",
    vig_inicial: "",
    vig_final: "",
    perfil_id: "",
    status: "",
    password: ""
  };

  componentDidMount = async () => {
    const response = await api.get("perfil");
    this.setState({
      perfil: response.data
    });
  };

  adicionar = e => {
    e.preventDefault();
    try {
      api.post("usuario", {
        nome: this.state.nome,
        username: this.state.username,
        email: this.state.email,
        cpf: this.state.cpf,
        nomeacao: this.state.nomeacao,
        vig_inicial: this.state.vig_inicial,
        vig_final: this.state.vig_final,
        perfil_id: this.state.perfil_id,
        status: this.state.status,
        password: this.state.password
      });
      this.setState({
        nome: "",
        username: "",
        email: "",
        cpf: "",
        nomeacao: "",
        vig_inicial: "",
        vig_final: "",
        perfil_id: "",
        status: "",
        password: ""
      });
      this.props.alert.success("Realizado com sucesso");
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <Link to="/usuarios" className="btn btn-warning">
            <i className="material-icons">format_align_justify</i>
          </Link>
          <div className="card">
            <div className="card-header card-header-success">
              <h4 className="card-title">Novo Usuário</h4>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label className="bmd-label-floating">Perfil</label>
                    <select
                      className="custom-select"
                      onChange={e =>
                        this.setState({ perfil_id: e.target.value })
                      }
                      value={this.state.perfil_id}
                    >
                      <option value="">Selecione o perfil</option>
                      {this.state.perfil.map(item => (
                        <option value={item.id}>{item.nome}</option>
                      ))}
                    </select>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <label className="bmd-label-floating">Status</label>
                    <select
                      className="custom-select"
                      onChange={e => this.setState({ status: e.target.value })}
                      value={this.state.status}
                    >
                      <option value="1">Ativo</option>
                      <option value="2">Inativo</option>
                    </select>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <label className="bmd-label-floating">Nome</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e => this.setState({ nome: e.target.value })}
                      value={this.state.nome}
                    />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <label className="bmd-label-floating">Login</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e =>
                        this.setState({ username: e.target.value })
                      }
                      value={this.state.username}
                    />
                  </div>
                </div>

                <div className="col-md-8">
                  <div className="form-group">
                    <label className="bmd-label-floating">E-mail</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e => this.setState({ email: e.target.value })}
                      value={this.state.email}
                    />
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label className="bmd-label-floating">CPF</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e => this.setState({ cpf: e.target.value })}
                      value={this.state.cpf}
                    />
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label className="bmd-label-floating">Doc. Nomeação</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e =>
                        this.setState({ nomeacao: e.target.value })
                      }
                      value={this.state.nomeacao}
                    />
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label className="bmd-label-floating">Vig. inicial</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e =>
                        this.setState({ vig_inicial: e.target.value })
                      }
                      value={this.state.vig_inicial}
                    />
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label className="bmd-label-floating">Vig. final</label>
                    <input
                      type="text"
                      className="form-control"
                      onChange={e =>
                        this.setState({ vig_final: e.target.value })
                      }
                      value={this.state.vig_final}
                    />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <label className="bmd-label-floating">Criar Senha</label>
                    <input
                      type="password"
                      className="form-control"
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                      value={this.state.password}
                    />
                  </div>
                </div>

                <div className="col-md-12">
                  <button
                    className="btn btn-info float-right"
                    onClick={this.adicionar}
                  >
                    Cadastrar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withAlert(UsuarioNovo);

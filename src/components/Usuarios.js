import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import { withAlert } from "react-alert";

import api from "../api";
import Data from "../data";

class Usuario extends React.Component {
  state = {
    usuarios: []
  };

  componentDidMount = async () => {
    const res = await api.get("usuario");
    this.setState({
      usuarios: res.data
    });
    console.log(this.props.match.path.replace("/", ""));
  };

  apagar = id => {
    try {
      api.delete(`usuario/${id}`);
      this.setState({
        usuarios: this.state.usuarios.filter(item => item.id !== id)
      });
      this.props.alert.success("Realizado com sucesso");
    } catch (err) {}
  };

  render() {
    return (
      <div className="content animated fadeIn">
        <div className="container-fluid">
          <Link to="usuarios/novo" className="btn btn-success">
            <i className="material-icons">add_circle</i>
          </Link>
          <div className="card">
            <div className="card-header card-header-success">
              <h4 className="card-title">Usuários</h4>
            </div>
            <div className="card-body">
              <div className="table-responsive">
                <table className="table table-hover">
                  <thead className="text-warning">
                    <tr>
                      <th>Nome</th>
                      <th>Login</th>
                      <th>Email</th>
                      <th>CPF</th>
                      <th>Doc. Nomeação</th>
                      <th>Vig. Inicical</th>
                      <th>Vig. Final</th>
                      <th>Perfil</th>
                      <th>Status</th>
                      <th>Ação</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.usuarios.map(list => (
                      <tr key={list.id}>
                        <td>{list.nome}</td>
                        <td>{list.username}</td>
                        <td>{list.email}</td>
                        <td>{list.cpf}</td>
                        <td>{list.nomeacao}</td>
                        <td>{moment(list.vig_inicial).format("DD/MM/YYYY")}</td>
                        <td>{moment(list.vig_final).format("DD/MM/YYYY")}</td>
                        <td>{list.perfils.nome}</td>
                        <td>{list.status === 0 ? "INATIVO" : "ATIVO"}</td>
                        <td>
                          <Link
                            to="/usuarios/editar/1"
                            className="text-success"
                          >
                            <i className="material-icons">edit</i>
                          </Link>
                          <a
                            className="text-success"
                            style={{ cursor: "pointer" }}
                            onClick={() => {
                              if (window.confirm("Deseja realmente apagar?")) {
                                this.apagar(list.id);
                              }
                              return;
                            }}
                          >
                            <i className="material-icons">delete</i>
                          </a>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withAlert(Usuario);

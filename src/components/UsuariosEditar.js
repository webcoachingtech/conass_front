import React from "react";
import { Link } from "react-router-dom";

const UsuariosEditar = props => {
  return (
    <div className="content animated fadeIn">
      <div className="container-fluid">
        <Link to="/usuarios" className="btn btn-warning">
          <i className="material-icons">format_align_justify</i>
        </Link>
        <div className="card">
          <div className="card-header card-header-success">
            <h4 className="card-title">Editar usuário</h4>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label className="bmd-label-floating">Nome</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label className="bmd-label-floating">Login</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-8">
                <div className="form-group">
                  <label className="bmd-label-floating">E-mail</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-4">
                <div className="form-group">
                  <label className="bmd-label-floating">CPF</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-4">
                <div className="form-group">
                  <label className="bmd-label-floating">Doc. Nomeação</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-4">
                <div className="form-group">
                  <label className="bmd-label-floating">Vig. inicial</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-4">
                <div className="form-group">
                  <label className="bmd-label-floating">Vig. final</label>
                  <input type="text" className="form-control" />
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label className="bmd-label-floating">Perfil</label>
                  <select className="custom-select">
                    <option value="">Selecione o perfil</option>
                    <option value="1">Aprovador</option>
                    <option value="2">Visualizador</option>
                  </select>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label className="bmd-label-floating">Status</label>
                  <select className="custom-select">
                    <option value="1">Ativo</option>
                    <option value="2">Inativo</option>
                  </select>
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label className="bmd-label-floating">Senha</label>
                  <input type="password" className="form-control" />
                </div>
              </div>

              <div className="col-md-6">
                <div className="form-group">
                  <label className="bmd-label-floating">Confirmar senha</label>
                  <input type="password" className="form-control" />
                </div>
              </div>

              <div className="col-md-12">
                <button className="btn btn-info float-right">Cadastrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UsuariosEditar;

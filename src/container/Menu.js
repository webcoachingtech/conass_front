import React from "react";

const Menu = [
  {
    name: "Dashboard",
    path: "/",
    icon: "dashboard"
  },
  {
    name: "Usuários",
    path: "/usuarios",
    icon: "person"
  },
  {
    name: "Perfil",
    path: "/perfil",
    icon: "check_circle"
  },
  {
    name: "Borderô",
    path: "/bordero",
    icon: "line_weight"
  }
];
export default Menu;

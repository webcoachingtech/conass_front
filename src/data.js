import React from "react";

const data = {
  table: [
    {
      id: "1",
      lote: "0000000",
      data: "12/03/2018",
      itemContabil: "Folha de pagamento",
      status: "Aprovado",
      emitente: "Claudia"
    },
    {
      id: "2",
      lote: "01010101",
      data: "01/03/2016",
      itemContabil: "13º salário",
      status: "Aguardando",
      emitente: "Fulano"
    }
  ],
  usuarios: [
    {
      nome: "Fulano de tal",
      login: "fulano",
      email: "fulano@fulano.com",
      cpf: "00000000000",
      nomeacao: "Ata nº 233",
      vig_inicial: "12/12/2018",
      vig_final: "12/12/2020",
      perfil: "Visualizador",
      status: "ativo"
    }
  ]
};
export default data;
